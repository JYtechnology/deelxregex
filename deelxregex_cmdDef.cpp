#include "include_deelxregex_header.h"

// 本命令被隐藏, 原始名字 = "构造", 本命令为构造函数
// 调用格式: _SDT_NULL (正则表达式DEELX).构造, 命令说明: "创建正则对象"
// 无参数
DEELXREGEX_EXTERN_C void deelxregex_Create_0_deelxregex(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{

}

// 本命令被隐藏, 原始名字 = "析构", 本命令为析构函数
// 调用格式: _SDT_NULL (正则表达式DEELX).析构, 命令说明: "释放正则对象"
// 无参数
DEELXREGEX_EXTERN_C void deelxregex_Release_1_deelxregex(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{

}

// 调用格式: SDT_BOOL (正则表达式DEELX).创建, 命令说明: "根据正则表达式语法，对正则表达式文本进行编译。"
// 参数<1>: 正则表达式文本 SDT_TEXT, 参数说明: "比如“易语言5\\.0(模块|支持库)?”"
// 参数<2>: [匹配模式 SDT_INT], 参数说明: "支持的匹配模式有：单行模式、多行模式、全局模式、忽略大小写、从右向左、扩展模式 这 6 种模式以及它们的组合。\r\n可以用如下常量（#正则常量.单行模式；#正则常量.多行模式；#正则常量.全局模式；#正则常量.忽略大小写；#正则常量.从右向左；#正则常量.扩展模式）可以查看支持库常量说明"
// 参数<3>: [支持转义符 SDT_BOOL], 参数说明: "默认为假，为真时支持易语言的部分常量/转义符：#换行符、#引号、#左引号、#右引号"
DEELXREGEX_EXTERN_C void deelxregex_Create_2_deelxregex(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{
    LPSTR    arg1 = pArgInf[1].m_pText;
    INT      arg2 = pArgInf[2].m_int;
    BOOL     arg3 = pArgInf[3].m_bool;

}

// 本命令被隐藏, 原始名字 = "构造", 本命令为构造函数
// 调用格式: _SDT_NULL (搜索结果DEELX).构造, 命令说明: "创建正则表达式对象"
// 无参数
DEELXREGEX_EXTERN_C void deelxregex_Create_3_deelxregex(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{

}

// 本命令被隐藏, 原始名字 = "析构", 本命令为析构函数
// 调用格式: _SDT_NULL (搜索结果DEELX).析构, 命令说明: "释放正则表达式对象"
// 无参数
DEELXREGEX_EXTERN_C void deelxregex_Release_4_deelxregex(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{

}

// 调用格式: MAKELONG(0x02, 0) (正则表达式DEELX).查找匹配, 命令说明: "通过 “搜索结果” 对象，可以得知是否匹配成功。如果成功，通过 “搜索结果” 对象可以获取捕获信息。"
// 参数<1>: [用来匹配的文本 SDT_TEXT], 参数说明: "进行匹配的字符串"
// 参数<2>: [开始查找匹配的位置 SDT_INT], 参数说明: "开始查找匹配的位置."
DEELXREGEX_EXTERN_C void deelxregex_Match_5_deelxregex(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{
    LPSTR    arg1 = pArgInf[1].m_pText;
    INT      arg2 = pArgInf[2].m_int;

}

// 调用格式: SDT_BOOL (搜索结果DEELX).是否为空, 命令说明: "如果本对象的内容为空，返回真；否则返回假。"
// 无参数
DEELXREGEX_EXTERN_C void deelxregex_IsEmpty_6_deelxregex(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{

}

// 调用格式: SDT_INT (搜索结果DEELX).获取开始位置, 命令说明: "匹配成功后，获取所匹配到的子字符串的开始位置。如果匹配失败，则返回负值。注意：本支持库内部采用Unicode编码，返回的位置也是相对于Unicode字符串的。"
// 无参数
DEELXREGEX_EXTERN_C void deelxregex_GetStart_7_deelxregex(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{

}

// 调用格式: SDT_INT (搜索结果DEELX).获取结束位置, 命令说明: "匹配成功后，获取所匹配到的子字符串的结束位置。如果匹配失败，则返回负值。注意：本支持库内部采用Unicode编码，返回的位置也是相对于Unicode字符串的。"
// 无参数
DEELXREGEX_EXTERN_C void deelxregex_GetEnd_8_deelxregex(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{

}

// 调用格式: SDT_INT (搜索结果DEELX).获取分组开始位置, 命令说明: "返回指定分组捕获的字符串的开始位置。如果指定分组未捕获，则返回负值。注意：本支持库内部采用Unicode编码，返回的位置也是相对于Unicode字符串的。"
// 参数<1>: 分组编号 SDT_INT, 参数说明: "分组编号"
DEELXREGEX_EXTERN_C void deelxregex_GetGroupStart_9_deelxregex(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{
    INT      arg1 = pArgInf[1].m_int;

}

// 调用格式: SDT_INT (搜索结果DEELX).获取分组结束位置, 命令说明: "返回指定分组捕获的字符串的结束位置。如果指定分组未捕获，则返回负值。注意：本支持库内部采用Unicode编码，返回的位置也是相对于Unicode字符串的。"
// 参数<1>: 分组编号 SDT_INT, 参数说明: "分组编号"
DEELXREGEX_EXTERN_C void deelxregex_GetGroupEnd_10_deelxregex(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{
    INT      arg1 = pArgInf[1].m_int;

}

// 调用格式: SDT_INT (搜索结果DEELX).取最大分组编号, 命令说明: "获取正则表达式最大捕获组编号。返回最大分组编号。"
// 无参数
DEELXREGEX_EXTERN_C void deelxregex_MaxGroupNumber_11_deelxregex(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{

}

// 调用格式: MAKELONG(0x02, 0) (正则表达式DEELX).绝对匹配, 命令说明: "通过 “搜索结果” 对象，可以得知是否匹配成功。如果成功，通过 “搜索结果” 对象可以获取捕获信息。"
// 参数<1>: [用来匹配的文本 SDT_TEXT], 参数说明: "进行匹配的字符串"
DEELXREGEX_EXTERN_C void deelxregex_MatchExact_12_deelxregex(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{
    LPSTR    arg1 = pArgInf[1].m_pText;

}

// 调用格式: SDT_INT (正则表达式DEELX).获取命名分组编号, 命令说明: "通过命名分组名，返回命名分组编号。"
// 参数<1>: 命名分组名 SDT_TEXT, 参数说明: "命名分组名"
DEELXREGEX_EXTERN_C void deelxregex_GetNamedGroupNumber_13_deelxregex(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{
    LPSTR    arg1 = pArgInf[1].m_pText;

}

// 本命令被隐藏, 原始名字 = "对象匹配"
// 调用格式: MAKELONG(0x02, 0) (正则表达式DEELX).对象匹配, 命令说明: "经过初始化的查找匹配上下文对象，用来在 “对象匹配” 中使用。"
// 参数<1>: [用来匹配的文本 SDT_TEXT], 参数说明: "进行匹配的字符串"
// 参数<2>: [开始查找匹配的位置 SDT_INT], 参数说明: "开始查找匹配的位置."
DEELXREGEX_EXTERN_C void deelxregex_PrepareMatch_14_deelxregex(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{
    LPSTR    arg1 = pArgInf[1].m_pText;
    INT      arg2 = pArgInf[2].m_int;

}

// 调用格式: SDT_TEXT (正则表达式DEELX).替换, 命令说明: "进行文本替换操作。返回替换后的文本。"
// 参数<1>: 欲被替换的文本 SDT_TEXT, 参数说明: "被进行替换的初始文本。"
// 参数<2>: [用作替换的文本 SDT_TEXT], 参数说明: "“替换为”字符串，将匹配到的子字符串替换成 此变量的 字符串。"
// 参数<3>: [起始替换位置 SDT_INT], 参数说明: "进行查找替换的开始位置。留空默认(-1)表示根据是否“从右向左(RIGHTTOLEFT)”自动决定开始位置。"
// 参数<4>: [替换进行的次数 SDT_INT], 参数说明: "指定进行替换的次数。留空默认(-1)表示替换所有匹配。"
DEELXREGEX_EXTERN_C void deelxregex_Replace_15_deelxregex(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{
    LPSTR    arg1 = pArgInf[1].m_pText;
    LPSTR    arg2 = pArgInf[2].m_pText;
    INT      arg3 = pArgInf[3].m_int;
    INT      arg4 = pArgInf[4].m_int;

}

// 本命令被隐藏, 原始名字 = "释放字符串"
// 调用格式: SDT_BOOL (正则表达式DEELX).释放字符串, 命令说明: "释放字符串"
// 参数<1>: [由(替换)返回的字符串 SDT_TEXT], 参数说明: "释放由 \"替换\" 返回的字符串。"
DEELXREGEX_EXTERN_C void deelxregex_ReleaseString_16_deelxregex(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{
    LPSTR    arg1 = pArgInf[1].m_pText;

}

// 调用格式: SDT_TEXT (正则表达式DEELX).取表达式文本, 命令说明: "返回以文本形式表示的正则表达式。如果该对象尚未创建，则返回空文本。"
// 无参数
DEELXREGEX_EXTERN_C void deelxregex_GetRegExText_17_deelxregex(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{

}

// 调用格式: SDT_TEXT (正则表达式DEELX).取结果文本, 命令说明: "取搜索结果的文本"
// 参数<1>: 搜索结果起始位置 SDT_INT, 参数说明: "搜索结果起始位置"
// 参数<2>: 搜索结果结束位置 SDT_INT, 参数说明: "搜索结果结束位置"
DEELXREGEX_EXTERN_C void deelxregex_GetResultText_18_deelxregex(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{
    INT      arg1 = pArgInf[1].m_int;
    INT      arg2 = pArgInf[2].m_int;

}

// 调用格式: SDT_INT (搜索结果DEELX).取子表达式个数, 命令说明: "返回该表达式中子表达式（用圆括号标记）的个数。（本结果和取最大分组编号相同,只不过是为了更兼容原支持库增加的）"
// 无参数
DEELXREGEX_EXTERN_C void deelxregex_GetSubExpCount_19_deelxregex(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{

}

// 调用格式: SDT_TEXT (搜索结果DEELX).取匹配文本, 命令说明: "取得与整个正则表达式匹配的子文本。"
// 无参数
DEELXREGEX_EXTERN_C void deelxregex_GetMatchText_20_deelxregex(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{

}

// 调用格式: SDT_TEXT (搜索结果DEELX).取子匹配文本, 命令说明: "取得与正则表达式中某个子表达式匹配的子文本。"
// 参数<1>: 子表达式索引或名称 _SDT_ALL, 参数说明: "欲取其值的子表达式。该参数对应与正则表达式中的一个子表达式（以圆括号标记）。子匹配索引从1开始，0表示匹配文本。\r\n\t或者传递文本型的子表达式分组名称，比如“(?<数字>\\d+)(?<小写字母>[a-z]+)(?<大写字母>[A-Z]+)”这样的表达式，可以传递“数字”、“小写字母”等名称。\r\n\t也可以传递字节集形式的参数，当成Unicode格式的名称对待。"
DEELXREGEX_EXTERN_C void deelxregex_GetSubMatchText_21_deelxregex(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{
    PVOID    arg1 = pArgInf[1].m_pByte;

}

// 调用格式: SDT_INT (搜索结果DEELX).是否匹配, 命令说明: "是否匹配成功。返回非零值表示匹配成功，返回 0 表示匹配失败。"
// 无参数
DEELXREGEX_EXTERN_C void deelxregex_IsMatched_22_deelxregex(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{

}

// 调用格式: MAKELONG(0x02, 0) (正则表达式DEELX).搜索, 命令说明: "使用指定的正则表达式搜索指定文本中与该表达式匹配的子文本。本命令和“查找匹配”一样，为兼容原支持库增加。"
// 参数<1>: [用来匹配的文本 SDT_TEXT], 参数说明: "进行匹配的字符串"
// 参数<2>: [开始查找匹配的位置 SDT_INT], 参数说明: "开始查找匹配的位置."
DEELXREGEX_EXTERN_C void deelxregex_Search_23_deelxregex(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{
    LPSTR    arg1 = pArgInf[1].m_pText;
    INT      arg2 = pArgInf[2].m_int;

}

// 调用格式: MAKELONG(0x02, 0) (正则表达式DEELX).搜索下一个, 命令说明: "本命令和“搜索”相似，只是自动偏移开始查找匹配的位置。"
// 无参数
DEELXREGEX_EXTERN_C void deelxregex_SearchNext_24_deelxregex(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{

}

// 调用格式: MAKELONG(0x02, 0) (正则表达式DEELX).[]搜索全部, 命令说明: "使用指定的正则表达式搜索指定文本中与该表达式匹配的所有子文本。返回值包含所有的搜索结果的一维数组，数组原有内容将被销毁，维数也将根据需要做相应调整。本命令的内部是通过循环多次调用“搜索”，每次指定适当的参数，来实现搜索整个文本的。返回值数组的各个成员分别对应每次调用“搜索”的返回值。本命令可高效地一次性取得目标文本中所有的匹配子文本信息。"
// 参数<1>: [用来匹配的文本 SDT_TEXT], 参数说明: "进行匹配的字符串"
// 参数<2>: [开始查找匹配的位置 SDT_INT], 参数说明: "开始查找匹配的位置."
DEELXREGEX_EXTERN_C void deelxregex_SearchAll_25_deelxregex(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{
    LPSTR    arg1 = pArgInf[1].m_pText;
    INT      arg2 = pArgInf[2].m_int;

}

// 调用格式: SDT_BOOL (正则表达式DEELX).创建W, 命令说明: "根据正则表达式语法，对正则表达式文本进行编译。"
// 参数<1>: 正则表达式文本 SDT_BIN, 参数说明: "Unicode格式的正则表达式文本，比如“易语言5\\.0(模块|支持库)?”"
// 参数<2>: [匹配模式 SDT_INT], 参数说明: "支持的匹配模式有：单行模式、多行模式、全局模式、忽略大小写、从右向左、扩展模式 这 6 种模式以及它们的组合。\r\n可以用如下常量（#正则常量.单行模式；#正则常量.多行模式；#正则常量.全局模式；#正则常量.忽略大小写；#正则常量.从右向左；#正则常量.扩展模式）可以查看支持库常量说明"
DEELXREGEX_EXTERN_C void deelxregex_CreateW_26_deelxregex(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{
    LPBYTE   arg1 = pArgInf[1].m_pBin;
    INT      arg2 = pArgInf[2].m_int;

}

// 调用格式: MAKELONG(0x02, 0) (正则表达式DEELX).查找匹配W, 命令说明: "通过 “搜索结果” 对象，可以得知是否匹配成功。如果成功，通过 “搜索结果” 对象可以获取捕获信息。"
// 参数<1>: [用来匹配的文本 SDT_BIN], 参数说明: "Unicode格式，进行匹配的字符串"
// 参数<2>: [开始查找匹配的位置 SDT_INT], 参数说明: "开始查找匹配的位置."
DEELXREGEX_EXTERN_C void deelxregex_MatchW_27_deelxregex(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{
    LPBYTE   arg1 = pArgInf[1].m_pBin;
    INT      arg2 = pArgInf[2].m_int;

}

// 调用格式: MAKELONG(0x02, 0) (正则表达式DEELX).搜索W, 命令说明: "使用指定的正则表达式搜索指定文本中与该表达式匹配的子文本。本命令和“查找匹配”一样，为兼容原支持库增加。"
// 参数<1>: [用来匹配的文本 SDT_BIN], 参数说明: "Unicode格式，进行匹配的字符串"
// 参数<2>: [开始查找匹配的位置 SDT_INT], 参数说明: "开始查找匹配的位置."
DEELXREGEX_EXTERN_C void deelxregex_SearchW_28_deelxregex(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{
    LPBYTE   arg1 = pArgInf[1].m_pBin;
    INT      arg2 = pArgInf[2].m_int;

}

// 调用格式: MAKELONG(0x02, 0) (正则表达式DEELX).[]搜索全部W, 命令说明: "使用指定的正则表达式搜索指定文本中与该表达式匹配的所有子文本。返回值包含所有的搜索结果的一维数组，数组原有内容将被销毁，维数也将根据需要做相应调整。本命令的内部是通过循环多次调用“搜索”，每次指定适当的参数，来实现搜索整个文本的。返回值数组的各个成员分别对应每次调用“搜索”的返回值。本命令可高效地一次性取得目标文本中所有的匹配子文本信息。"
// 参数<1>: [用来匹配的文本 SDT_BIN], 参数说明: "Unicode格式，进行匹配的字符串"
// 参数<2>: [开始查找匹配的位置 SDT_INT], 参数说明: "开始查找匹配的位置."
DEELXREGEX_EXTERN_C void deelxregex_SearchAllW_29_deelxregex(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{
    LPBYTE   arg1 = pArgInf[1].m_pBin;
    INT      arg2 = pArgInf[2].m_int;

}

// 调用格式: SDT_BIN (正则表达式DEELX).替换W, 命令说明: "进行文本替换操作。返回替换后的文本(Unicode格式文本，字节集类型)。"
// 参数<1>: 欲被替换的文本 SDT_BIN, 参数说明: "Unicode格式，被进行替换的初始文本。"
// 参数<2>: [用作替换的文本 SDT_BIN], 参数说明: "Unicode格式，“替换为”字符串，将匹配到的子字符串替换成 此变量的 字符串。"
// 参数<3>: [起始替换位置 SDT_INT], 参数说明: "进行查找替换的开始位置。留空默认(-1)表示根据是否“从右向左(RIGHTTOLEFT)”自动决定开始位置。"
// 参数<4>: [替换进行的次数 SDT_INT], 参数说明: "指定进行替换的次数。留空默认(-1)表示替换所有匹配。"
DEELXREGEX_EXTERN_C void deelxregex_ReplaceW_30_deelxregex(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{
    LPBYTE   arg1 = pArgInf[1].m_pBin;
    LPBYTE   arg2 = pArgInf[2].m_pBin;
    INT      arg3 = pArgInf[3].m_int;
    INT      arg4 = pArgInf[4].m_int;

}

// 调用格式: SDT_BIN (正则表达式DEELX).取表达式文本W, 命令说明: "返回以文本形式表示的正则表达式(Unicode格式文本，字节集类型)。如果该对象尚未创建，则返回空文本。"
// 无参数
DEELXREGEX_EXTERN_C void deelxregex_GetRegExTextW_31_deelxregex(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{

}

// 调用格式: SDT_BIN (正则表达式DEELX).取结果文本W, 命令说明: "取搜索结果的文本(Unicode格式文本，字节集类型)。"
// 参数<1>: 搜索结果起始位置 SDT_INT, 参数说明: "搜索结果起始位置"
// 参数<2>: 搜索结果结束位置 SDT_INT, 参数说明: "搜索结果结束位置"
DEELXREGEX_EXTERN_C void deelxregex_GetResultTextW_32_deelxregex(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{
    INT      arg1 = pArgInf[1].m_int;
    INT      arg2 = pArgInf[2].m_int;

}

// 调用格式: SDT_BIN (搜索结果DEELX).取匹配文本W, 命令说明: "取得与整个正则表达式匹配的子文本(Unicode格式文本，字节集类型)。"
// 无参数
DEELXREGEX_EXTERN_C void deelxregex_GetMatchTextW_33_deelxregex(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{

}

// 调用格式: SDT_BIN (搜索结果DEELX).取子匹配文本W, 命令说明: "取得与正则表达式中某个子表达式匹配的子文本(Unicode格式文本，字节集类型)。"
// 参数<1>: 子表达式索引或名称 _SDT_ALL, 参数说明: "欲取其值的子表达式。该参数对应与正则表达式中的一个子表达式（以圆括号标记）。子匹配索引从1开始，0表示匹配文本。\r\n\t或者传递文本型的子表达式分组名称，比如“(?<数字>\\d+)(?<小写字母>[a-z]+)(?<大写字母>[A-Z]+)”这样的表达式，可以传递“数字”、“小写字母”等名称。\r\n\t也可以传递字节集形式的参数，当成Unicode格式的名称对待。"
DEELXREGEX_EXTERN_C void deelxregex_GetSubMatchTextW_34_deelxregex(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{
    PVOID    arg1 = pArgInf[1].m_pByte;

}

// 调用格式: MAKELONG(0x02, 0) (正则表达式DEELX).绝对匹配W, 命令说明: "通过 “搜索结果” 对象，可以得知是否匹配成功。如果成功，通过 “搜索结果” 对象可以获取捕获信息。"
// 参数<1>: [用来匹配的文本 SDT_BIN], 参数说明: "Unicode格式，进行匹配的字符串"
DEELXREGEX_EXTERN_C void deelxregex_MatchExactW_35_deelxregex(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{
    LPBYTE   arg1 = pArgInf[1].m_pBin;

}

// 调用格式: SDT_BOOL (正则表达式DEELX).测试匹配, 命令说明: "测试表达式与匹配文本是否完全匹配，成功返回真，失败返回假。该方法常用于判断用户输入数据的合法性，比如检验Email的合法性。"
// 参数<1>: [用来匹配的文本 SDT_TEXT], 参数说明: "进行匹配的字符串"
DEELXREGEX_EXTERN_C void deelxregex_Test_36_deelxregex(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{
    LPSTR    arg1 = pArgInf[1].m_pText;

}

// 调用格式: SDT_BOOL (正则表达式DEELX).测试匹配W, 命令说明: "测试表达式与匹配文本是否完全匹配，成功返回真，失败返回假。该方法常用于判断用户输入数据的合法性，比如检验Email的合法性。"
// 参数<1>: [用来匹配的文本 SDT_BIN], 参数说明: "Unicode格式，进行匹配的字符串"
DEELXREGEX_EXTERN_C void deelxregex_TestW_37_deelxregex(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{
    LPBYTE   arg1 = pArgInf[1].m_pBin;

}

// 调用格式: SDT_TEXT (正则表达式DEELX).[]分割, 命令说明: "使用指定的正则表达式将指定文本进行分割。返回分割后的一维文本数组。如果失败则返回一个空数组，即没有任何成员的数组。"
// 参数<1>: 待分割文本 SDT_TEXT, 参数说明: NULL
// 参数<2>: [要返回的子文本数目 SDT_INT], 参数说明: "如果被省略或等于0，则默认返回所有的子文本"
DEELXREGEX_EXTERN_C void deelxregex_Split_38_deelxregex(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{
    LPSTR    arg1 = pArgInf[1].m_pText;
    INT      arg2 = pArgInf[2].m_int;

}

// 调用格式: SDT_BIN (正则表达式DEELX).[]分割W, 命令说明: "使用指定的正则表达式将指定文本进行分割。返回分割后的一维字节集数组。如果失败则返回一个空数组，即没有任何成员的数组。"
// 参数<1>: 待分割文本 SDT_BIN, 参数说明: "Unicode格式文本"
// 参数<2>: [要返回的子文本数目 SDT_INT], 参数说明: "如果被省略或等于0，则默认返回所有的子文本"
DEELXREGEX_EXTERN_C void deelxregex_SplitW_39_deelxregex(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{
    LPBYTE   arg1 = pArgInf[1].m_pBin;
    INT      arg2 = pArgInf[2].m_int;

}

// 调用格式: SDT_TEXT (正则表达式DEELX).[]取所有匹配文本, 命令说明: "取得与整个正则表达式匹配的文本。返回匹配到的一维文本数组，本命令不处理正则表达式中括号里的子文本，和“分割”命令相似，但结果不同。如果失败则返回一个空数组，即没有任何成员的数组。"
// 参数<1>: 用来匹配的文本 SDT_TEXT, 参数说明: NULL
// 参数<2>: [要返回的匹配文本数目 SDT_INT], 参数说明: "如果被省略或等于0，则默认返回所有的匹配文本"
DEELXREGEX_EXTERN_C void deelxregex_GetAllMatchText_40_deelxregex(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{
    LPSTR    arg1 = pArgInf[1].m_pText;
    INT      arg2 = pArgInf[2].m_int;

}

// 调用格式: SDT_BIN (正则表达式DEELX).[]取所有匹配文本W, 命令说明: "取得与整个正则表达式匹配的文本。返回匹配到的一维字节集数组，本命令不处理正则表达式中括号里的子文本，和“分割”命令相似，但结果不同。如果失败则返回一个空数组，即没有任何成员的数组。"
// 参数<1>: 用来匹配的文本 SDT_BIN, 参数说明: "Unicode格式文本"
// 参数<2>: [要返回的匹配文本数目 SDT_INT], 参数说明: "如果被省略或等于0，则默认返回所有的匹配文本"
DEELXREGEX_EXTERN_C void deelxregex_GetAllMatchTextW_41_deelxregex(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{
    LPBYTE   arg1 = pArgInf[1].m_pBin;
    INT      arg2 = pArgInf[2].m_int;

}

// 本命令被隐藏, 原始名字 = "复制构造", 本命令为复制构造函数
// 调用格式: _SDT_NULL (正则表达式DEELX).复制构造, 命令说明: "复制构造正则表达式对象"
// 无参数
DEELXREGEX_EXTERN_C void deelxregex_Copy_42_deelxregex(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{

}

// 本命令被隐藏, 原始名字 = "复制构造", 本命令为复制构造函数
// 调用格式: _SDT_NULL (搜索结果DEELX).复制构造, 命令说明: "复制构造搜索结果对象"
// 无参数
DEELXREGEX_EXTERN_C void deelxregex_Copy_43_deelxregex(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{

}

